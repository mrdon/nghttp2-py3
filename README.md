Builds a docker container with:

- Ubuntu 15.04
- nghttp2 git master
- Python 3.4 with pip and venv

To build:
  
```
docker build -t mrdonbrown/nghttp2-py3 .
```