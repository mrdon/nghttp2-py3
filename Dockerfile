FROM ubuntu:vivid

MAINTAINER Don Brown <https://bitbucket.org/mrdon/nghttp2-py3>

# Setup HTTP/2 nghttp2 on Ubuntu 15.x
RUN ulimit -c -m -s -t unlimited && \  
    apt-get update && \
    apt-get install -y libssh2-1 libssh2-1-dev iputils-ping jq libc6-dev bison mercurial libboost-dev libboost-thread-dev vim tar bsdmainutils apt-file wget mlocate make binutils autoconf automake autotools-dev libtool pkg-config zlib1g-dev libcunit1-dev libssl-dev libxml2-dev libev-dev libevent-dev libjansson-dev libjemalloc-dev openssl git gcc g++ libpcre3-dev libcap-dev libncurses5-dev curl cython python3.4-dev python3-pip python3-venv gdb python3-dbg && \
    apt-get clean && \
    apt-get autoclean && \
    apt-get remove  

#RUN cd /usr/local/src && \
#    wget -cnv https://www.python.org/ftp/python/3.4.3/Python-3.4.3.tar.xz && \
#    tar xf Python-3.4.3.tar.xz && \
#    cd Python-3.4.3 && \
#    ./configure --prefix=/usr && \
#    make && \
#    make install && \
#    ldconfig && \
#    make clean

RUN cd /usr/local/src  && \
    git clone --depth 1 https://github.com/tatsuhiro-t/nghttp2.git && \
    cd /usr/local/src/nghttp2 && \
    autoreconf -i && \
    automake && \ 
    autoconf && \
    ./configure PYTHON=/usr/bin/python3 --enable-app --disable-examples && \
    make -j2 && \
    make install && \
    ldconfig && \
    make clean


